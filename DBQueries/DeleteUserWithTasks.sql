CREATE PROCEDURE DeleteUserWithTasks
@Id INT
AS
DELETE [dbo].[Note] WHERE UserId = @Id
DELETE [dbo].[User] WHERE [dbo].[User].[Id] = @Id