CREATE DATABASE "NotebookTaskDb" COLLATE Cyrillic_General_CI_AS;
USE "NotebookTaskDb";

CREATE TABLE "Role"
(
	Id INT PRIMARY KEY IDENTITY,
	RoleName NVARCHAR(100) NOT NULL
);

CREATE TABLE "User"
(
	Id INT PRIMARY KEY IDENTITY,
	RoleId INT DEFAULT 1 NOT NULL,
	UserLogin NVARCHAR(100) NOT NULL,
	UserEmail NVARCHAR(100) NOT NULL,
	UserPassword NVARCHAR(100) NOT NULL,
	FOREIGN KEY (RoleId) REFERENCES "Role" (Id)
);

CREATE TABLE "Category"
(
	Id INT PRIMARY KEY IDENTITY,
	CategoryName NVARCHAR(100) NOT NULL
);

CREATE TABLE "Image"
(
	Id INT PRIMARY KEY IDENTITY,
	ImageData NVARCHAR(500) NOT NULL
);

CREATE TABLE "Note"
(
	Id INT PRIMARY KEY IDENTITY,
	UserId INT NOT NULL,
	ImageId INT NULL,
	CategoryId INT NULL,
	NoteReferenceId INT NULL,
	NoteName NVARCHAR(100) NOT NULL,
	NoteCreatedDate DATETIME NOT NULL,
	NoteDescription NVARCHAR(500) NULL,
	FOREIGN KEY (UserId) REFERENCES "User" (Id),
	FOREIGN KEY (ImageId) REFERENCES "Image" (Id)
	ON DELETE SET NULL
	ON UPDATE CASCADE,
	FOREIGN KEY (CategoryId) REFERENCES "Category" (Id)
	ON DELETE SET NULL
	ON UPDATE CASCADE,
);

CREATE TABLE "NoteReference"
(
	Id INT PRIMARY KEY IDENTITY,
	NoteId INT NOT NULL,
	NoteLinkId INT NOT NULL,
	FOREIGN KEY (NoteId) REFERENCES "Note" (Id),
	FOREIGN KEY (NoteLinkId) REFERENCES "Note" (Id)
);

CREATE PROCEDURE DeleteUserWithTasks
@Id INT
AS
DELETE [dbo].[Note] WHERE UserId = @Id
DELETE [dbo].[User] WHERE [dbo].[User].[Id] = @Id

INSERT [dbo].[Category] ([CategoryName]) VALUES ('Health')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Study')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Journey')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Спорт')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Hobby')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Programming')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Purchases')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Clothing')
INSERT [dbo].[Category] ([CategoryName]) VALUES ('Food')

INSERT [dbo].[Image] ([ImageData]) VALUES ('bdac3da5-e447-4d46-a155-fcc87c28ee63-rFtBUUxukt4.jpg')
INSERT [dbo].[Image] ([ImageData]) VALUES ('2e1efca1-6a6c-4c8e-9904-18a906811423-корова.jpg')
INSERT [dbo].[Image] ([ImageData]) VALUES ('a0150434-554c-420a-83a0-a4b3ff25e16b-W-n97mCSoaQ.jpg')
INSERT [dbo].[Image] ([ImageData]) VALUES ('d19589d3-7944-4820-9af4-059b28a062b0-8.jpg')
INSERT [dbo].[Image] ([ImageData]) VALUES ('6c739605-76c6-437a-8f0c-000ea08bf724-cmRXShEnDNc.jpg')
INSERT [dbo].[Image] ([ImageData]) VALUES ('markeloff.jpg')

INSERT [dbo].[Role] ([RoleName]) VALUES ('User')
INSERT [dbo].[Role] ([RoleName]) VALUES ('Editor')
INSERT [dbo].[Role] ([RoleName]) VALUES ('Admin')

INSERT [dbo].[User] ([RoleId], [UserLogin], [UserEmail], [UserPassword]) VALUES (1, 'deserof', 'axarkevich@gmail.com', '123')
INSERT [dbo].[User] ([RoleId], [UserLogin], [UserEmail], [UserPassword]) VALUES (1, 'TestUser', 'user@mail.ru', N'123')
INSERT [dbo].[User] ([RoleId], [UserLogin], [UserEmail], [UserPassword]) VALUES (1, 'deserof123', 'deserof123@gmail.com', 'agfdgsdfgsdf')
INSERT [dbo].[User] ([RoleId], [UserLogin], [UserEmail], [UserPassword]) VALUES (3, 'admin', 'admin@gmail.com', 'admin')

INSERT [dbo].[Note] ([UserId], [ImageId], [CategoryId], [NoteReferenceId], [NoteName], [NoteCreatedDate], [NoteDescription]) VALUES (1, 1, 4, NULL, 'Турники', CAST(N'2021-05-30T23:38:19.650' AS DateTime), 'Пойти на турники')
INSERT [dbo].[Note] ([UserId], [ImageId], [CategoryId], [NoteReferenceId], [NoteName], [NoteCreatedDate], [NoteDescription]) VALUES (2, 2, 1, 0, 'Купить яблоки', CAST(N'2001-01-01T00:00:00.000' AS DateTime), 'в магаззине')
INSERT [dbo].[Note] ([UserId], [ImageId], [CategoryId], [NoteReferenceId], [NoteName], [NoteCreatedDate], [NoteDescription]) VALUES (3, 3, 9, 0, 'Покупки', CAST(N'2021-06-06T22:36:00.000' AS DateTime), 'в магаззине')
