﻿using Moq;
using NotebookTask.Bll.Services.Implementations;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NotebookTask.Tests.DataAccessLayerTests
{
    public class UserRepositoryTests
    {
        [Fact]
        public void GetById_WhenIdHasData_ShouldReturnUser()
        {
            // Arrange
            var users = new List<User>
            {
                new User {Id = 5, RoleId = 2, UserEmail = "mail@mail.ru", UserLogin = "cheburek", UserPassword = "12345"},
                new User {Id = 4, RoleId = 2, UserEmail = "artem@gmail.com", UserLogin = "adminLeha", UserPassword = "12345"},
                new User {Id = 2, RoleId = 1, UserEmail = "sdigital@mail.ru", UserLogin = "login3", UserPassword = "12345"},

            };
            var mock = new Mock<IUserRepository>();
            mock.Setup(x => x.GetById(It.IsAny<int>())).Returns((int i) => users.Single(bo => bo.Id == i));
            var sut = new UserService(mock.Object);

            // Act
            var result = sut.GetById(2);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Id);
            Assert.Equal("login3", result.UserLogin);
        }
    }
}
