﻿namespace NotebookTask.Dal.Helpers
{
    public interface IDbHelper
    {
        string GetConnectionString();
    }
}
