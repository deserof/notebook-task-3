﻿using NotebookTask.Common.Configs;

namespace NotebookTask.Dal.Helpers
{
    public class DbHelper : IDbHelper
    {
        private readonly DbConfig options;

        public DbHelper(DbConfig options)
        {
            this.options = options;
        }

        public string GetConnectionString()
        {
            return options.ConnectionString;
        }
    }
}
