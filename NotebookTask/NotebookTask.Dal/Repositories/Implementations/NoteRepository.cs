﻿using NotebookTask.Common.Models;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NotebookTask.Dal.Repositories.Implementations
{
    public class NoteRepository : INoteRepository
    {
        private readonly IDbHelper dbHelper;

        public NoteRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public Note Create(Note item)
        {
            string sqlExpression = @"INSERT INTO Note (UserId, ImageId, CategoryId, NoteReferenceId, NoteName, NoteCreatedDate, NoteDescription) 
VALUES (@UserId, @ImageId, @CategoryId, @NoteReferenceId, @NoteName, @NoteCreatedDate, @NoteDescription)";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserId", item.UserId);
                command.Parameters.AddWithValue("@ImageId", item.ImageId);
                command.Parameters.AddWithValue("@CategoryId", item.CategoryId);
                command.Parameters.AddWithValue("@NoteReferenceId", item.NoteReferenceId);
                command.Parameters.AddWithValue("@NoteName", item.NoteName);
                command.Parameters.AddWithValue("@NoteCreatedDate", item.NoteCreatedDate);
                command.Parameters.AddWithValue("@NoteDescription", item.NoteDescription);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public bool Delete(int id)
        {
            string sqlExpression = "DELETE FROM Note WHERE Id = @Id";

            try
            {
                using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Note> GetAll()
        {
            List<Note> notes = new List<Note>();
            string sqlExpression = "SELECT * FROM Note ORDER BY Id";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Note note = new Note
                        {
                            Id = (int)reader["Id"],
                            UserId = (int)reader["UserId"],
                            ImageId = (reader["ImageId"] is DBNull) ? null : (int?)reader["ImageId"],
                            CategoryId = (reader["CategoryId"] is DBNull) ? null : (int?)reader["CategoryId"],
                            NoteReferenceId = (reader["NoteReferenceId"] is DBNull) ? null : (int?)reader["NoteReferenceId"],
                            NoteName = (string)reader["NoteName"],
                            NoteCreatedDate = (DateTime)reader["NoteCreatedDate"],
                            NoteDescription = (string)reader["NoteDescription"]
                        };

                        notes.Add(note);
                    }
                }

                reader.Close();
            }

            return notes;
        }

        public Note GetById(int id)
        {
            var note = new Note();
            string sqlExpression = "SELECT * FROM Note WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        note.Id = (int)reader["Id"];
                        note.UserId = (int)reader["UserId"];
                        note.ImageId = (int)reader["ImageId"];
                        note.CategoryId = (int)reader["CategoryId"];
                        note.NoteReferenceId = (int)reader["NoteReferenceId"];
                        note.NoteName = (string)reader["NoteName"];
                        note.NoteCreatedDate = (DateTime)reader["NoteCreatedDate"];
                        note.NoteDescription = (string)reader["NoteDescription"];
                    }
                }

                reader.Close();
            }

            return note;
        }

        public IEnumerable<Note> GetByUserId(int id)
        {
            List<Note> notes = new List<Note>();
            string sqlExpression = "SELECT * FROM Note WHERE UserId = @UserId";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                command.Parameters.AddWithValue("@UserId", id);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Note note = new Note
                        {
                            Id = (int)reader["Id"],
                            UserId = (int)reader["UserId"],
                            ImageId = (reader["ImageId"] is DBNull) ? null : (int?)reader["ImageId"],
                            CategoryId = (reader["CategoryId"] is DBNull) ? null : (int?)reader["CategoryId"],
                            NoteReferenceId = (reader["NoteReferenceId"] is DBNull) ? null : (int?)reader["NoteReferenceId"],
                            NoteName = (string)reader["NoteName"],
                            NoteCreatedDate = (DateTime)reader["NoteCreatedDate"],
                            NoteDescription = (string)reader["NoteDescription"]
                        };

                        notes.Add(note);
                    }
                }

                reader.Close();
            }

            return notes;
        }

        public string GetImageDataById(int id)
        {
            string imageData = string.Empty;
            string sqlExpression = "SELECT ImageData FROM Note JOIN [dbo].[Image] ON Note.ImageId = [dbo].[Image].[Id] WHERE Note.Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        imageData = reader["ImageData"] is DBNull ? null : (string)reader["ImageData"];
                    }
                }

                reader.Close();
            }

            return imageData;
        }

        public Note Update(int id, Note item)
        {
            string sqlExpression = @"UPDATE Note Set UserId = @UserId, ImageId = @ImageId, CategoryId = @CategoryId,
NoteReferenceId = @NoteReferenceId, NoteName = @NoteName, NoteCreatedDate = @NoteCreatedDate, NoteDescription = @NoteDescription
WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserId", item.UserId);
                command.Parameters.AddWithValue("@ImageId", item.ImageId);
                command.Parameters.AddWithValue("@CategoryId", item.CategoryId);
                command.Parameters.AddWithValue("@NoteReferenceId", item.NoteReferenceId);
                command.Parameters.AddWithValue("@NoteName", item.NoteName);
                command.Parameters.AddWithValue("@NoteCreatedDate", item.NoteCreatedDate);
                command.Parameters.AddWithValue("@NoteDescription", item.NoteDescription);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return item;
        }
    }
}
