﻿using NotebookTask.Common.Models;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NotebookTask.Dal.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IDbHelper dbHelper;

        public CategoryRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public Category Create(Category item)
        {
            string sqlExpression = "INSERT INTO Category (CategoryName) VALUES (@CategoryName)";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@CategoryName", item.CategoryName);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public bool Delete(int id)
        {
            string sqlExpression = "DELETE FROM Category WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return true;
        }

        public IEnumerable<Category> GetAll()
        {
            List<Category> categories = new List<Category>();

            string sqlExpression = "SELECT * FROM Category ORDER BY Id";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Category category = new Category
                        {
                            Id = (int)reader["Id"],
                            CategoryName = (string)reader["CategoryName"]
                        };

                        categories.Add(category);
                    }
                }

                reader.Close();
            }

            return categories;
        }

        public Category GetById(int id)
        {
            var category = new Category();
            string sqlExpression = $"SELECT * FROM Category WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        category.Id = (int)reader["Id"];
                        category.CategoryName = (string)reader["CategoryName"];
                    }
                }

                reader.Close();
            }

            return category;
        }

        public Category Update(int id, Category item)
        {
            string sqlExpression = "UPDATE Category Set CategoryName = @CategoryName WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@CategoryName", id);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return item;
        }
    }
}
