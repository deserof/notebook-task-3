﻿using NotebookTask.Common.Models;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NotebookTask.Dal.Repositories.Implementations
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDbHelper dbHelper;

        public RoleRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public Role Create(Role item)
        {
            string sqlExpression = "INSERT INTO Role (RoleName) VALUES (@RoleName)";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@RoleName", item.RoleName);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public bool Delete(int id)
        {
            string sqlExpression = "DELETE FROM Role WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return true;
        }

        public IEnumerable<Role> GetAll()
        {
            List<Role> roles = new List<Role>();

            string sqlExpression = "SELECT * FROM Role";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Role role = new Role
                        {
                            Id = (int)reader["Id"],
                            RoleName = (string)reader["RoleName"]
                        };

                        roles.Add(role);
                    }
                }

                reader.Close();
            }

            return roles;
        }

        public Role GetById(int id)
        {
            var role = new Role();
            string sqlExpression = "SELECT * FROM Role WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        role.Id = (int)reader["Id"];
                        role.RoleName = (string)reader["RoleName"];
                    }
                }

                reader.Close();
            }

            return role;
        }

        public Role Update(int id, Role item)
        {
            string sqlExpression = "UPDATE Role Set RoleName = @RoleName WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@RoleName", item.RoleName);
                command.Parameters.AddWithValue("@Id",id);
                command.ExecuteNonQuery();
            }

            return item;
        }
    }
}
