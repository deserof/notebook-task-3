﻿using NotebookTask.Common.Models;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NotebookTask.Dal.Repositories.Implementations
{
    public class ImageRepository : IImageRepository
    {
        private readonly IDbHelper dbHelper;

        public ImageRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public Image Create(Image item)
        {
            string sqlExpression = $"INSERT INTO Image (ImageData)" +
                                   $" VALUES (@ImageData)";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@ImageData", item.ImageData);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public bool Delete(int id)
        {
            string sqlExpression = $"DELETE FROM Image WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return true;
        }

        public IEnumerable<Image> GetAll()
        {
            List<Image> images = new List<Image>();

            string sqlExpression = "SELECT * FROM Image";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Image image = new Image
                        {
                            Id = (int)reader["Id"],
                            ImageData = (string)reader["ImageData"]
                        };

                        images.Add(image);
                    }
                }

                reader.Close();
            }

            return images;
        }

        public Image GetById(int id)
        {
            var image = new Image();
            string sqlExpression = $"SELECT * FROM Image WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        image.Id = (int)reader["Id"];
                        image.ImageData = (string)reader["ImageData"];
                    }
                }

                reader.Close();
            }

            return image;
        }

        public int? GetIdByImageData(string imageData)
        {
            int? id = null;
            string sqlExpression = $"Select Id FROM [dbo].[Image] Where ImageData = @ImageData";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@ImageData", imageData);
                command.ExecuteNonQuery();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = (reader["Id"] is DBNull) ? null : (int?)reader["Id"];
                    }
                }

                reader.Close();
            }

            return id;
        }

        public Image Update(int id, Image item)
        {
            string sqlExpression = $"UPDATE Image Set ImageData = @ImageData WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@ImageData", item.ImageData);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return item;
        }
    }
}
