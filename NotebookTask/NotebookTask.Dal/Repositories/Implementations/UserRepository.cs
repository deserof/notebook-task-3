﻿using NotebookTask.Common.Models;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NotebookTask.Dal.Repositories.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly IDbHelper dbHelper;

        public UserRepository(IDbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public User Create(User item)
        {
            string sqlExpression = @"INSERT INTO [dbo].[User] (UserLogin, UserEmail, UserPassword, RoleId)
VALUES(@UserLogin, @UserEmail, @UserPassword, @RoleId)";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserLogin", item.UserLogin);
                command.Parameters.AddWithValue("@UserEmail", item.UserEmail);
                command.Parameters.AddWithValue("@UserPassword", item.UserPassword);
                command.Parameters.AddWithValue("@RoleId", item.RoleId);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public bool Delete(int id)
        {
            string sqlExpression = "DeleteUserWithTasks";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter sqlParameter = new SqlParameter
                {
                    ParameterName = "@Id",
                    Value = id
                };
                command.Parameters.Add(sqlParameter);
                command.ExecuteReader();
            }

            return true;
        }

        public IEnumerable<User> GetAll()
        {
            List<User> users = new List<User>();

            string sqlExpression = "SELECT * FROM [dbo].[User]";

            using (SqlConnection connect = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connect.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connect);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        User user = new User
                        {
                            Id = (int)reader["Id"],
                            RoleId = (int)reader["RoleId"],
                            UserLogin = (string)reader["UserLogin"],
                            UserEmail = (string)reader["UserEmail"],
                            UserPassword = (string)reader["UserPassword"]
                        };

                        users.Add(user);
                    }
                }

                reader.Close();
            }

            return users;
        }

        public User GetById(int id)
        {
            var user = new User();
            string sqlExpression = "SELECT * FROM [dbo].[User] WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader["Id"];
                        user.RoleId = (int)reader["RoleId"];
                        user.UserLogin = (string)reader["UserLogin"];
                        user.UserEmail = (string)reader["UserEmail"];
                        user.UserPassword = (string)reader["UserPassword"];
                    }
                }

                reader.Close();
            }

            return user;
        }

        public User Update(int id, User item)
        {
            string sqlExpression = @"UPDATE [dbo].[User] SET RoleId = @RoleId, UserLogin = @UserLogin, UserEmail = @UserEmail,
UserPassword = @UserPassword WHERE Id = @Id";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@RoleId", item.RoleId);
                command.Parameters.AddWithValue("@UserLogin", item.UserLogin);
                command.Parameters.AddWithValue("@UserEmail", item.UserEmail);
                command.Parameters.AddWithValue("@UserPassword", item.UserPassword);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
            }

            return item;
        }

        public User GetByEmail(string email)
        {
            var user = new User();
            string sqlExpression = "SELECT * FROM [dbo].[User] WHERE UserEmail = @UserEmail";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserEmail", email);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader["Id"];
                        user.RoleId = (int)reader["RoleId"];
                        user.UserLogin = (string)reader["UserLogin"];
                        user.UserEmail = (string)reader["UserEmail"];
                        user.UserPassword = (string)reader["UserPassword"];
                    }
                }

                reader.Close();
            }

            return user;
        }

        public string GetRoleByEmail(string email)
        {
            string userRole = string.Empty;

            string sqlExpression = @"SELECT [dbo].[Role].[RoleName] 
FROM [dbo].[User] JOIN [dbo].[Role] ON [dbo].[User].[RoleId] = [dbo].[Role].[Id]
WHERE [dbo].[User].[UserEmail] = @UserEmail";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserEmail", email);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        userRole = (string)reader["RoleName"];
                    }
                }

                reader.Close();
            }

            return userRole;
        }

        public bool IsEmailExist(string email)
        {
            bool isEmailExist = false;

            string sqlExpression = "SELECT [dbo].[User].[UserEmail] FROM [dbo].[User] WHERE [dbo].[User].[UserEmail] = @UserEmail";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserEmail", email);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        isEmailExist = true;
                    }
                }

                reader.Close();
            }

            return isEmailExist;
        }

        public User GetByLogin(string login)
        {
            var user = new User();
            string sqlExpression = $"SELECT * FROM [dbo].[User] WHERE UserLogin = @UserLogin";

            using (SqlConnection connection = new SqlConnection(dbHelper.GetConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@UserLogin", login);
                command.ExecuteNonQuery();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader["Id"];
                        user.RoleId = (int)reader["RoleId"];
                        user.UserLogin = (string)reader["UserLogin"];
                        user.UserEmail = (string)reader["UserEmail"];
                        user.UserPassword = (string)reader["UserPassword"];
                    }
                }

                reader.Close();
            }

            return user;
        }
    }
}
