﻿using NotebookTask.Common.Models;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
