﻿using NotebookTask.Common.Models;
using System.Collections.Generic;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface INoteRepository : IRepository<Note>
    {
        string GetImageDataById(int id);

        IEnumerable<Note> GetByUserId(int id);
    }
}
