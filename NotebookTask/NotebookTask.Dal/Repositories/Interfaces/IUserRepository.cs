﻿using NotebookTask.Common.Models;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        public User GetByEmail(string email);

        public User GetByLogin(string login);

        public string GetRoleByEmail(string email);

        public bool IsEmailExist(string email);
    }
}
