﻿using NotebookTask.Common.Models;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
