﻿using System.Collections.Generic;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        T Create(T item);

        T Update(int id, T item);

        bool Delete(int id);
    }
}
