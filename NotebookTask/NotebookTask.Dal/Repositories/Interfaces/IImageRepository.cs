﻿using NotebookTask.Common.Models;

namespace NotebookTask.Dal.Repositories.Interfaces
{
    public interface IImageRepository : IRepository<Image>
    {
        int? GetIdByImageData(string imageData);
    }
}
