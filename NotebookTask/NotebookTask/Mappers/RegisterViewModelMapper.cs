﻿using NotebookTask.Common.Models;
using NotebookTask.ViewModels;

namespace NotebookTask.Mappers
{
    public static class RegisterViewModelMapper
    {
        public static User ToCommon(this RegisterViewModel registerViewModel)
        {
            return new User
            {
                UserEmail = registerViewModel.Email,
                UserLogin = registerViewModel.Login,
                UserPassword = registerViewModel.Password
            };
        }
    }
}