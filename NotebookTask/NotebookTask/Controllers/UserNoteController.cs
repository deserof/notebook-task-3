﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace NotebookTask.Controllers
{
    [Authorize]
    public class UserNoteController : Controller
    {
        private readonly INoteService noteService;
        private readonly ICategoryService categoryService;
        private readonly IUserService userService;
        private readonly IImageService imageService;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UserNoteController(INoteService noteService, ICategoryService categoryService, IImageService imageService,
            IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            this.imageService = imageService;
            this.categoryService = categoryService;
            this.userService = userService;
            this.noteService = noteService;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult List(string categoryName)
        {
            SelectList categories = new SelectList(categoryService.GetAll().ToList(), "Id", "CategoryName");
            ViewBag.Categories = categories;
            var listNoteViewModels = new List<ListNoteViewModel>();
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var id = userService.GetByEmail(userEmail).Id;
            ListNoteViewModel listNoteViewModel;
            var userNotes = noteService.GetByUserId(id);

            foreach (var note in userNotes)
            {
                listNoteViewModel = new ListNoteViewModel
                {
                    Id = note.Id,
                    Category = categoryService.GetById(note.CategoryId is null ? 0 : (int)note.CategoryId).CategoryName,
                    User = userService.GetById(note.UserId).UserLogin,
                    Image = imageService.GetById(note.ImageId is null ? 0 : (int)note.ImageId).ImageData,
                    Name = note.NoteName,
                    CreatedDate = note.NoteCreatedDate,
                    Description = note.NoteDescription
                };

                listNoteViewModels.Add(listNoteViewModel);
            }

            if (categoryName is null)
            {
                return View(listNoteViewModels);
            }

            var notes = listNoteViewModels.Where(x => x.Category.Equals(categoryName));

            return View(notes);
        }

        [HttpPost]
        public IActionResult List(int? categoryId)
        {
            if (categoryId is null)
            {
                return RedirectToAction(nameof(List));
            }

            string categoryName = categoryService.GetById((int)categoryId).CategoryName;

            return RedirectToAction(nameof(List), new { categoryName = categoryName });
        }

        [HttpPost]
        public IActionResult Create(CreateNoteViewModel model)
        {
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var id = userService.GetByEmail(userEmail).Id;

            string imageData = UploadImage(model.ImageData);

            Image image = new Image
            {
                ImageData = imageData,
            };

            imageService.Create(image);

            Note note = new Note
            {
                UserId = id,
                CategoryId = model.Category,
                NoteName = model.Name,
                NoteDescription = model.Description,
                ImageId = imageService.GetIdByImageData(imageData),
                NoteReferenceId = 0,
                NoteCreatedDate = DateTime.Now
            };

            noteService.Create(note);

            return RedirectToAction(nameof(List));
        }

        [HttpPost]
        private string UploadImage(IFormFile imageData)
        {
            return (imageData != null) ? imageService.UploadImage(imageData) : null;
        }

        [HttpGet]
        public IActionResult Create()
        {
            SelectList categories = new SelectList(categoryService.GetAll().ToList(), "Id", "CategoryName");
            ViewBag.Categories = categories;

            return View();
        }

        public IActionResult Delete(int id)
        {
            noteService.Delete(id);

            return RedirectToAction(nameof(List));
        }

        [HttpGet]
        public IActionResult Edit()
        {
            SelectList categories = new SelectList(categoryService.GetAll().ToList(), "Id", "CategoryName");
            ViewBag.Categories = categories;

            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, EditNoteViewModel model)
        {
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var UserId = userService.GetByEmail(userEmail).Id;
            string imageData = string.Empty;

            if (model.ImageData != null)
            {
                if (model.ImageData.FileName != noteService.GetImageDataById(id))
                {
                    imageData = UploadImage(model.ImageData);

                    Image image = new Image
                    {
                        ImageData = imageData,
                    };

                    imageService.Create(image);
                }
            }

            Note note = new Note
            {
                Id = id,
                CategoryId = model.Category,
                ImageId = imageService.GetIdByImageData(imageData),
                NoteCreatedDate = model.CreatedDate,
                NoteDescription = model.Description,
                NoteName = model.Name,
                UserId = UserId,
                NoteReferenceId = 0
            };

            noteService.Update(id, note);

            return RedirectToAction(nameof(List));
        }

        [HttpPost]
        public IActionResult Search(string text, DateTime? begin, DateTime? end)
        {
            ViewData["GetText"] = text;

            var listNoteViewModels = new List<ListNoteViewModel>();
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var id = userService.GetByEmail(userEmail).Id;
            ListNoteViewModel listNoteViewModel;
            var userNotes = noteService.GetByUserId(id);

            foreach (var note in userNotes)
            {
                listNoteViewModel = new ListNoteViewModel
                {
                    Id = note.Id,
                    Category = categoryService.GetById(note.CategoryId is null ? 0 : (int)note.CategoryId).CategoryName,
                    User = userService.GetById(note.UserId).UserLogin,
                    Image = imageService.GetById(note.ImageId is null ? 0 : (int)note.ImageId).ImageData,
                    Name = note.NoteName,
                    CreatedDate = note.NoteCreatedDate,
                    Description = note.NoteDescription
                };

                listNoteViewModels.Add(listNoteViewModel);
            }

            if (!string.IsNullOrWhiteSpace(text))
            {
                var notesWithText = from x in listNoteViewModels where x.Name.Contains(text) || x.Category.Contains(text) select x;

                return View(notesWithText);
            }

            if (!(begin is null || end is null))
            {
                var notes = from x in listNoteViewModels where x.CreatedDate >= begin && x.CreatedDate <= end select x;

                return View(notes);
            }

            return View(listNoteViewModels);
        }

        [HttpGet]
        public IActionResult Search()
        {
            var listNoteViewModels = new List<ListNoteViewModel>();
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var id = userService.GetByEmail(userEmail).Id;
            ListNoteViewModel listNoteViewModel;
            var userNotes = noteService.GetByUserId(id);

            foreach (var note in userNotes)
            {
                listNoteViewModel = new ListNoteViewModel
                {
                    Id = note.Id,
                    Category = categoryService.GetById(note.CategoryId is null ? 0 : (int)note.CategoryId).CategoryName,
                    User = userService.GetById(note.UserId).UserLogin,
                    Image = imageService.GetById(note.ImageId is null ? 0 : (int)note.ImageId).ImageData,
                    Name = note.NoteName,
                    CreatedDate = note.NoteCreatedDate,
                    Description = note.NoteDescription
                };

                listNoteViewModels.Add(listNoteViewModel);
            }

            return View(listNoteViewModels);
        }
    }
}
