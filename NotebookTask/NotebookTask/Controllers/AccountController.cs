﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Extensions;
using NotebookTask.Mappers;
using NotebookTask.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace NotebookTask.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly IUserService userService;
        
        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = userService.GetByEmail(model.Email);

                if (model.Password.Equals(user.UserPassword))
                {
                    Authenticate(user.UserEmail, userService.GetRoleByEmail(user.UserEmail));

                    if (userService.GetRoleByEmail(model.Email).Equals("Admin"))
                    {

                        return Redirect(this.Action<AdminController>(nameof(AdminController.Index)));
                    }

                    if (userService.GetRoleByEmail(model.Email).Equals("Editor"))
                    {

                        return Redirect(this.Action<EditorController>(nameof(EditorController.Index)));
                    }

                    return Redirect(this.Action<UserNoteController>(nameof(UserNoteController.List)));
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel model)
        {
            string loginPattern = @"[a-zA-Z][a-zA-Z0-9]{3,15}";
            string emailPattern = @"[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*";
            string passwordPattern = @"(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}";

            if (!Regex.IsMatch(model.Email is null ? "" : model.Email, emailPattern))
            {
                ModelState.AddModelError(nameof(model.Email), "Invalid email");
            }

            if (!Regex.IsMatch(model.Login is null ? "" : model.Login, loginPattern))
            {
                ModelState.AddModelError(nameof(model.Login), "Invalid login. Minimum 4 characters");
            }

            if (!Regex.IsMatch(model.Password is null ? "" : model.Password, passwordPattern))
            {
                ModelState.AddModelError(nameof(model.Password), "Invalid password. Minimum six characters, at least one letter and one number");
            }

            if (ModelState.IsValid)
            {
                User user = model.ToCommon();
                user.RoleId = 1;

                if (userService.GetByEmail(user.UserEmail).UserEmail is null && userService.GetByLogin(user.UserLogin).UserLogin is null)
                {
                    userService.Create(user);
                    Authenticate(user.UserEmail, userService.GetRoleByEmail(user.UserEmail));

                    return Redirect(this.Action<UserNoteController>(nameof(UserNoteController.List)));
                }

                ModelState.AddModelError("", "User already exists");
            }

            ModelState.AddModelError("", "Invalid data");

            return View(model);
        }

        private void Authenticate(string userName, string userRole)
        {
            var claims = new List<Claim>
            {
            new Claim(ClaimsIdentity.DefaultNameClaimType, userName),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, userRole)
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Redirect(this.Action<AccountController>(nameof(AccountController.Login)));
        }
    }
}