﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.ViewModels;

namespace NotebookTask.Controllers
{
    [Authorize(Roles = "Admin,Editor")]
    public class ImageController : Controller
    {
        private readonly IImageService imageService;
        private readonly IWebHostEnvironment webHostEnvironment;

        public ImageController(IImageService imageService, IWebHostEnvironment webHostEnvironment)
        {
            this.imageService = imageService;
            this.webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult List()
        {
            var images = imageService.GetAll();

            return View(images);
        }

        [HttpPost]
        public IActionResult Create(CreateImageViewModel model)
        {
            string imageData = UploadImage(model);

            Image image = new Image
            {
                ImageData = imageData,
            };

            imageService.Create(image);

            return RedirectToAction(nameof(List));
        }

        [HttpPost]
        private string UploadImage(CreateImageViewModel model)
        {
            return (model.ImageData != null) ? imageService.UploadImage(model.ImageData) : null;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Delete(int id)
        {
            imageService.Delete(id);

            return RedirectToAction(nameof(List));
        }

        [HttpGet]
        public IActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, CreateImageViewModel model)
        {
            string imageData = UploadImage(model);

            Image image = new Image
            {
                ImageData = imageData,
            };

            imageService.Update(id, image);         

            return RedirectToAction(nameof(List));
        }
    }
}
