﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Extensions;
using NotebookTask.ViewModels;
using System.Diagnostics;
using System.Security.Claims;

namespace NotebookTask.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IUserService userService;

        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor httpContextAccessor, IUserService userService)
        {
            _logger = logger;
            this.httpContextAccessor = httpContextAccessor;
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType) != null)
            {
                var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
                var userRole = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultRoleClaimType).Value;
                var id = userService.GetByEmail(userEmail).Id;

                if (userRole == "Admin")
                {
                    return Redirect(this.Action<AdminController>(nameof(AdminController.Index)));
                }

                if (userRole == "Editor")
                {
                    return Redirect(this.Action<EditorController> (nameof(EditorController.Index)));
                }

                return Redirect(this.Action<UserNoteController>(nameof(UserNoteController.List)));
            }

            return View();
        }

        [HttpGet]
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
