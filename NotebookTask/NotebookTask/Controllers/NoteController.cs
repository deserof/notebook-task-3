﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NotebookTask.Controllers
{
    [Authorize(Roles = "Admin,Editor")]
    public class NoteController : Controller
    {
        private readonly INoteService noteService;
        private readonly ICategoryService categoryService;
        private readonly IUserService userService;
        private readonly IImageService imageService;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly IHttpContextAccessor httpContextAccessor;

        public NoteController(INoteService noteService, ICategoryService categoryService, IImageService imageService,
            IUserService userService, IWebHostEnvironment webHostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            this.imageService = imageService;
            this.categoryService = categoryService;
            this.userService = userService;
            this.noteService = noteService;
            this.webHostEnvironment = webHostEnvironment;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult List()
        {
            var listNoteViewModels = new List<ListNoteViewModel>();
            var notes = noteService.GetAll();
            ListNoteViewModel listNoteViewModel;

            foreach (var note in notes)
            {
                listNoteViewModel = new ListNoteViewModel
                {
                    Id = note.Id,
                    Category = categoryService.GetById(note.CategoryId is null ? 0 : (int)note.CategoryId).CategoryName,
                    User = userService.GetById(note.UserId).UserLogin,
                    Image = imageService.GetById(note.ImageId is null ? 0 : (int)note.ImageId).ImageData,
                    Name = note.NoteName,
                    CreatedDate = note.NoteCreatedDate,
                    Description = note.NoteDescription
                };

                listNoteViewModels.Add(listNoteViewModel);
            }

            return View(listNoteViewModels);
        }

        [HttpPost]
        public IActionResult Create(CreateNoteViewModel model)
        {
            string imageData = UploadImage(model.ImageData);

            Image image = new Image
            {
                ImageData = imageData,
            };

            imageService.Create(image);

            Note note = new Note
            {
                UserId = model.User,
                CategoryId = model.Category,
                NoteName = model.Name,
                NoteDescription = model.Description,
                ImageId = imageService.GetIdByImageData(imageData),
                NoteReferenceId = 0,
                NoteCreatedDate = DateTime.Now
            };

            noteService.Create(note);

            return RedirectToAction(nameof(List));
        }

        [HttpPost]
        private string UploadImage(IFormFile imageData)
        {
            return (imageData != null) ? imageService.UploadImage(imageData) : null;
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            SelectList users = new SelectList(userService.GetAll().ToList(), "Id", "UserLogin");
            SelectList categories = new SelectList(categoryService.GetAll().ToList(), "Id", "CategoryName");
            ViewBag.Users = users;
            ViewBag.Categories = categories;
            return View();
        }

        public IActionResult Delete(int id)
        {
            noteService.Delete(id);

            return RedirectToAction(nameof(List));
        }

        [HttpGet]
        public IActionResult Edit()
        {
            SelectList users = new SelectList(userService.GetAll().ToList(), "Id", "UserLogin");
            SelectList categories = new SelectList(categoryService.GetAll().ToList(), "Id", "CategoryName");
            ViewBag.Users = users;
            ViewBag.Categories = categories;

            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, EditNoteViewModel model)
        {
            string imageData = string.Empty;

            if (model.ImageData != null)
            {
                if (model.ImageData.FileName != noteService.GetImageDataById(id))
                {
                    imageData = UploadImage(model.ImageData);

                    Image image = new Image
                    {
                        ImageData = imageData,
                    };

                    imageService.Create(image);
                }
            }

            Note note = new Note
            {
                Id = id,
                CategoryId = model.Category,
                ImageId = imageService.GetIdByImageData(imageData),
                NoteCreatedDate = model.CreatedDate,
                NoteDescription = model.Description,
                NoteName = model.Name,
                UserId = model.User,
                NoteReferenceId = 0
            };

            noteService.Update(id, note);

            return RedirectToAction(nameof(List));
        }
    }
}
