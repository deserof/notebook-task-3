﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Extensions;
using NotebookTask.ViewModels;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace NotebookTask.Controllers
{
    [Authorize]
    public class UserProfileController : Controller
    {
        private IUserService userService;
        private IHttpContextAccessor httpContextAccessor;

        public UserProfileController(IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            this.userService = userService;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Edit()
        {
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var userLogin = userService.GetByEmail(userEmail).UserLogin;
            var userRole = userService.GetRoleByEmail(userEmail);
            ViewData["UserEmail"] = userEmail;
            ViewData["UserLogin"] = userLogin;
            ViewData["UserRole"] = userRole;

            return View();
        }

        [HttpPost]
        public IActionResult Edit(EditUserViewModel editUserViewModel)
        {
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var userLogin = userService.GetByEmail(userEmail).UserLogin;
            var userRole = userService.GetRoleByEmail(userEmail);
            var id = userService.GetByEmail(userEmail).Id;
            string emailPattern = @"[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*";
            string passwordPattern = @"(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}";
            ViewData["UserEmail"] = userEmail;
            ViewData["UserLogin"] = userLogin;
            ViewData["UserRole"] = userRole;

            if (editUserViewModel is null)
            {
                RedirectToAction(nameof(Edit));
            }

            if (!(editUserViewModel.EditUserEmailViewModel is null))
            {
                if (!userService.IsEmailExist(editUserViewModel.EditUserEmailViewModel.Email))
                {
                    if (Regex.IsMatch(editUserViewModel.EditUserEmailViewModel.Email is null ? "" : editUserViewModel.EditUserEmailViewModel.Email, emailPattern))
                    {
                        User user = new User
                        {
                            Id = id,
                            RoleId = userService.GetById(id).RoleId,
                            UserEmail = editUserViewModel.EditUserEmailViewModel.Email,
                            UserLogin = userService.GetById(id).UserLogin,
                            UserPassword = userService.GetById(id).UserPassword
                        };

                        userService.Update(id, user);

                        return Redirect(this.Action<AccountController>(nameof(AccountController.Logout)));
                    }
                }
                ModelState.AddModelError(nameof(editUserViewModel.EditUserEmailViewModel), "Invalid email");
            }

            if (!(editUserViewModel.EditUserPasswordViewModel is null))
            {
                if (Regex.IsMatch(editUserViewModel.EditUserPasswordViewModel.Password is null ? "" : editUserViewModel.EditUserPasswordViewModel.Password, passwordPattern))
                {
                    User user = new User
                    {
                        Id = id,
                        RoleId = userService.GetById(id).RoleId,
                        UserEmail = userService.GetById(id).UserEmail,
                        UserLogin = userService.GetById(id).UserLogin,
                        UserPassword = editUserViewModel.EditUserPasswordViewModel.Password
                    };

                    userService.Update(id, user);

                    return Redirect(this.Action<AccountController>(nameof(AccountController.Logout)));
                }
                
                ModelState.AddModelError(nameof(editUserViewModel.EditUserPasswordViewModel), "Invalid password. Minimum six characters, at least one letter and one number");
            }

            return View();
        }
    }
}
