﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;

namespace NotebookTask.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IRoleService roleService;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UserController(IUserService userService, IRoleService roleService, IHttpContextAccessor httpContextAccessor)
        {
            this.userService = userService;
            this.roleService = roleService;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult List()
        {
            var users = userService.GetAll();
            List<UserViewModel> userViewModels = new List<UserViewModel>();

            foreach (var user in users)
            {
                UserViewModel userViewModel = new UserViewModel
                {
                    Id = user.Id,
                    Email = user.UserEmail,
                    Login = user.UserLogin,
                    Password = user.UserPassword,
                    Role = roleService.GetById(user.RoleId).RoleName
                };

                userViewModels.Add(userViewModel);
            }

            return View(userViewModels);
        }

        [HttpPost]
        public IActionResult Create(User model)
        {
            userService.Create(model);

            return RedirectToAction(nameof(List));
        }

        [HttpGet]
        public IActionResult Create()
        {
            SelectList roles = new SelectList(roleService.GetAll(), "Id", "RoleName");
            ViewBag.Roles = roles;
            return View();
        }

        public IActionResult Delete(int id)
        {
            var userEmail = httpContextAccessor.HttpContext.User.FindFirst(ClaimsIdentity.DefaultNameClaimType).Value;
            var UserId = userService.GetByEmail(userEmail).Id;

            if (id == UserId)
            {
                return RedirectToAction(nameof(List));
            }

            userService.Delete(id);

            return RedirectToAction(nameof(List));
        }

        [HttpGet]
        public IActionResult Edit()
        {
            SelectList roles = new SelectList(roleService.GetAll(), "Id", "RoleName");
            ViewBag.Roles = roles;

            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, User model)
        {
            userService.Update(id, model);

            return RedirectToAction(nameof(List));
        }
    }
}
