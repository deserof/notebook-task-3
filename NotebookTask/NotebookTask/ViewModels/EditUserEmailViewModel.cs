﻿namespace NotebookTask.ViewModels
{
    public class EditUserEmailViewModel
    {
        public string Email { get; set; }
    }
}
