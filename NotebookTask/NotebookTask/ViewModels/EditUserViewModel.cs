﻿namespace NotebookTask.ViewModels
{
    public class EditUserViewModel
    {
        public EditUserEmailViewModel EditUserEmailViewModel { get; set; }

        public EditUserPasswordViewModel EditUserPasswordViewModel { get; set; }
    }
}
