﻿using System.ComponentModel.DataAnnotations;

namespace NotebookTask.ViewModels
{
    public class EditUserPasswordViewModel
    {
        [Required(ErrorMessage = "Enter password")]
        public string Password { get; set; }

        [Required]
        [Compare(nameof(Password), ErrorMessage = "Password mismatch")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
    }
}
