﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace NotebookTask.ViewModels
{
    public class CreateImageViewModel
    {
        [Required(ErrorMessage = "Choose image")]
        [Display(Name = "image")]
        public IFormFile ImageData { get; set; }
    }
}
