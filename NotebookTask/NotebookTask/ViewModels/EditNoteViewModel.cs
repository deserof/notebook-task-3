﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace NotebookTask.ViewModels
{
    public class EditNoteViewModel
    {
        public int User { get; set; }

        public IFormFile ImageData { get; set; }

        public int Category { get; set; }

        public int NoteReferenceId { get; set; }

        [Required(ErrorMessage = "Enter name")]
        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = "Enter description")]
        public string Description { get; set; }
    }
}
