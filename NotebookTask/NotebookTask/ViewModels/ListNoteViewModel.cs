﻿using System;

namespace NotebookTask.ViewModels
{
    public class ListNoteViewModel
    {
        public int Id { get; set; }

        public string User { get; set; }

        public string Image { get; set; }

        public string Category { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }
    }
}
