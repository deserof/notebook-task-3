﻿namespace NotebookTask.ViewModels
{
    public class ListImageViewModel
    {
        public int Id { get; set; }

        public string ImageData { get; set; }
    }
}
