﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Controllers;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NotebookTask.Tests.ViewLayerTests
{
    public class CategoryControllerTests
    {
        [Fact]
        public void Index_ReturnsAView_ResultWithAListOfCategories()
        {
            // Arrange
            var mock = new Mock<ICategoryService>();
            mock.Setup(repo => repo.GetAll()).Returns(GetTestCategories());
            var sut = new CategoryController(mock.Object);

            // Act
            var result = sut.List();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Category>>(viewResult.Model);
            Assert.Equal(3, model.Count());
        }

        private IEnumerable<Category> GetTestCategories()
        {
            var categories = new List<Category>
            {
                new Category { Id = 1, CategoryName = "Sport" },
                new Category { Id = 2, CategoryName = "Coding" },
                new Category { Id = 3, CategoryName = "Knitting" },
            };

            return categories;
        }
    }
}
