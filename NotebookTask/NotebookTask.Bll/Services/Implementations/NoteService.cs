﻿using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Implementations
{
    public class NoteService : INoteService
    {
        private readonly INoteRepository noteRepository;

        public NoteService(INoteRepository noteRepository)
        {
            this.noteRepository = noteRepository;
        }

        public Note Create(Note item)
        {
            return noteRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return noteRepository.Delete(id);
        }

        public IEnumerable<Note> GetAll()
        {
            return noteRepository.GetAll();
        }

        public Note GetById(int id)
        {
            return noteRepository.GetById(id);
        }

        public IEnumerable<Note> GetByUserId(int id)
        {
            return noteRepository.GetByUserId(id);
        }

        public string GetImageDataById(int id)
        {
            return noteRepository.GetImageDataById(id);
        }

        public Note Update(int id, Note item)
        {
            return noteRepository.Update(id, item);
        }
    }
}
