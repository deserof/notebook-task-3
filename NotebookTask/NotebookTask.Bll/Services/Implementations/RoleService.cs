﻿using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        public Role Create(Role item)
        {
            return roleRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return roleRepository.Delete(id);
        }

        public IEnumerable<Role> GetAll()
        {
            return roleRepository.GetAll();
        }

        public Role GetById(int id)
        {
            return roleRepository.GetById(id);
        }

        public Role Update(int id, Role item)
        {
            return roleRepository.Update(id, item);
        }
    }
}
