﻿using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public Category Create(Category item)
        {
            return categoryRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return categoryRepository.Delete(id);
        }

        public IEnumerable<Category> GetAll()
        {
            return categoryRepository.GetAll();
        }

        public Category GetById(int id)
        {
            return categoryRepository.GetById(id);
        }

        public Category Update(int id, Category item)
        {
            return categoryRepository.Update(id, item);
        }
    }
}
