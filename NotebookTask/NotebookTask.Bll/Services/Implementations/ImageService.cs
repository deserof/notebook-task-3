﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace NotebookTask.Bll.Services.Implementations
{
    public class ImageService : IImageService
    {
        private readonly IImageRepository imageRepository;
        private readonly IWebHostEnvironment webHostEnvironment;

        public ImageService(IImageRepository imageRepository, IWebHostEnvironment webHostEnvironment)
        {
            this.imageRepository = imageRepository;
            this.webHostEnvironment = webHostEnvironment;
        }

        public Image Create(Image item)
        {
            return imageRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return imageRepository.Delete(id);
        }

        public IEnumerable<Image> GetAll()
        {
            return imageRepository.GetAll();
        }

        public Image GetById(int id)
        {
            return imageRepository.GetById(id);
        }

        public int? GetIdByImageData(string imageData)
        {
            return imageRepository.GetIdByImageData(imageData);
        }

        public Image Update(int id, Image item)
        {
            return imageRepository.Update(id, item);
        }

        public string UploadImage(IFormFile imageData)
        {
            string imageName;
            string imageDir = Path.Combine(webHostEnvironment.WebRootPath, "Images");
            imageName = imageData.FileName;
            string imagePath = Path.Combine(imageDir, imageName);
            using var imageStream = new FileStream(imagePath, FileMode.Create);
            imageData.CopyTo(imageStream);
            return imageName;
        }
    }
}
