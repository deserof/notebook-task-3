﻿using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Common.Models;
using NotebookTask.Dal.Repositories.Interfaces;
using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public User Create(User item)
        {
            return userRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return userRepository.Delete(id);
        }

        public IEnumerable<User> GetAll()
        {
            return userRepository.GetAll();
        }

        public User GetById(int id)
        {
            return userRepository.GetById(id);
        }

        public User Update(int id, User item)
        {
            return userRepository.Update(id, item);
        }

        public User GetByEmail(string email)
        {
            return userRepository.GetByEmail(email);
        }

        public string GetRoleByEmail(string email)
        {
            return userRepository.GetRoleByEmail(email);
        }

        public bool IsEmailExist(string email)
        {
            return userRepository.IsEmailExist(email);
        }

        public User GetByLogin(string login)
        {
            return userRepository.GetByLogin(login);
        }
    }
}
