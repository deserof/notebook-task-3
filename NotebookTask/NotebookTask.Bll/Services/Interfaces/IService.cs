﻿using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Interfaces
{
    public interface IService<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        T Create(T item);

        T Update(int id, T item);

        bool Delete(int id);
    }
}
