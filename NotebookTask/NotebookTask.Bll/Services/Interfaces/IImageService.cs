﻿using Microsoft.AspNetCore.Http;
using NotebookTask.Common.Models;

namespace NotebookTask.Bll.Services.Interfaces
{
    public interface IImageService : IService<Image>
    {
        int? GetIdByImageData(string imageData);

        string UploadImage(IFormFile imageData);
    }
}
