﻿using NotebookTask.Common.Models;

namespace NotebookTask.Bll.Services.Interfaces
{
    public interface IRoleService : IService<Role>
    {
    }
}
