﻿using NotebookTask.Common.Models;
using System.Collections.Generic;

namespace NotebookTask.Bll.Services.Interfaces
{
    public interface INoteService : IService<Note>
    {
        string GetImageDataById(int id);

        IEnumerable<Note> GetByUserId(int id);

        void Search();
    }
}
