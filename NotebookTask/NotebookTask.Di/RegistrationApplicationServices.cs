﻿using Microsoft.Extensions.DependencyInjection;
using NotebookTask.Bll.Services.Implementations;
using NotebookTask.Bll.Services.Interfaces;
using NotebookTask.Dal.Helpers;
using NotebookTask.Dal.Repositories.Implementations;
using NotebookTask.Dal.Repositories.Interfaces;

namespace NotebookTask.Di
{
    public static class RegistrationApplicationServices
    {
        public static void BuildDependencies(this IServiceCollection services)
        {
            // DbHelper
            services.AddSingleton<IDbHelper, DbHelper>();

            // Services
            services.AddSingleton<ICategoryService, CategoryService>();
            services.AddSingleton<IImageService, ImageService>();
            services.AddSingleton<INoteService, NoteService>();
            services.AddSingleton<IRoleService, RoleService>();
            services.AddSingleton<IUserService, UserService>();

            // Repositories
            services.AddSingleton<ICategoryRepository, CategoryRepository>();
            services.AddSingleton<IImageRepository, ImageRepository>();
            services.AddSingleton<INoteRepository, NoteRepository>();
            services.AddSingleton<IRoleRepository, RoleRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
        }
    }
}