﻿namespace NotebookTask.Common.Models
{
    public class NoteReference
    {
        public int Id { get; set; }

        public int NoteId { get; set; }

        public int NoteLinkId { get; set; }
    }
}
