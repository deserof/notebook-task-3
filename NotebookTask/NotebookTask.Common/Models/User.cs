﻿using System.ComponentModel.DataAnnotations;

namespace NotebookTask.Common.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public int RoleId { get; set; }

        [Required]
        public string UserLogin { get; set; }

        [Required]
        public string UserEmail { get; set; }

        [Required]
        public string UserPassword { get; set; }
    }
}
