﻿using System;

namespace NotebookTask.Common.Models
{
    public class Note
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int? ImageId { get; set; }

        public int? CategoryId { get; set; }

        public int? NoteReferenceId { get; set; }

        public string NoteName { get; set; }

        public DateTime NoteCreatedDate { get; set; }

        public string NoteDescription { get; set; }
    }
}
