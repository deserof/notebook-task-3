﻿namespace NotebookTask.Common.Models
{
    public class Image
    {
        public int Id { get; set; }

        public string ImageData { get; set; }
    }
}
